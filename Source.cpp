#include <iostream>

using namespace std;
template <typename T>
class STACK
{
private:
    T* stack;
    int count;
    int lastelem;

public:

    STACK()
    {
        stack = nullptr;
        count = 0;
        lastelem = 0;
    }

    void push(T item)
    {
        lastelem++;
        if (lastelem > count)
        {
            T* tmp;
            tmp = stack;
            stack = new T[count + 1];
            count++;
            for (int i = 0; i < count - 1; i++)
                stack[i] = tmp[i];
            stack[count - 1] = item;
            lastelem = count;
            if (count > 1)
                delete[] tmp;
        }
        else
        {
            stack[lastelem-1] = item;
        }
    }

    T pop()
    {
        lastelem--;
        if (count == 0)
            return 0;
        cout << "Deleted number = " << *(stack + count - 1) << endl;
        cout << endl;
        return stack[lastelem];
    }

    void Print()
    {
        T* p;
        p = stack;
        cout << "Stack: " << endl;
        for (int i = 0; i < lastelem; i++)
        {
            cout << "Item[" << i << "] = " << *p << endl;
            p++;
        }
        cout << endl;
    }
};

int main()
{
    STACK <int> st1;
    st1.Print();
    st1.push(59);
    st1.push(15);
    st1.push(4);
    st1.Print();
    st1.pop();
    st1.Print();
    st1.push(78);
    st1.Print();
}
